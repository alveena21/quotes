/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import { AppRegistry } from 'react-native';
import App from './App';

import Routes from './src/navigations'
import { name as appName } from './app.json';
import { LandingPage } from './src/container'
import TrackPlayer from 'react-native-track-player';

TrackPlayer.setupPlayer().then(() => {
    // The player is ready to be used
});
AppRegistry.registerComponent(appName, () => Routes);

TrackPlayer.registerPlaybackService(() => require('./service'));