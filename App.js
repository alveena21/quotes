/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import TrackPlayer from 'react-native-track-player';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
  componentDidMount() {
    TrackPlayer.setupPlayer().then(async () => {

      // Adds a track to the queue
      await TrackPlayer.add({

        id: 1111,
        url: "https://drive.google.com/uc?export=download&id=1AjPwylDJgR8DOnmJWeRgZzjsohi-7ekj",
        title: "Longing",
        artist: "David Chavez",
        artwork: "https://picsum.photos/200"

      });

      // Starts playing it


    });
  }
  play = () => {
    TrackPlayer.play();
  }
  stop = () => {
    TrackPlayer.pause();
  }
  render() {
    return (
      <View style={styles.container}>
        {/* <TouchableOpacity onPress={() => this.play()}>
          <Text>Play</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.stop()}>
          <Text>Pause</Text>
        </TouchableOpacity> */}
        <Image style={{width:100,height}} source={{ uri: 'https://picsum.photos/200' }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
