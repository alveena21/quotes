
/**
 *  import modules
 */
import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage, Dimensions } from 'react-native'


/**
 *  start of DrawerContent
 */
class DrawerContainer extends Component {

    // logout = async () => {
    //     const { navigate } = this.props.navigation
    //     AsyncStorage.clear().then(() => {
    //         this.props.logout()
    //         console.log(this.props.logout)
    //         this.props.navigation.navigate('Login')
    //     })
    //     OneSignal.deleteTag('email');
    //     this.props.logout()
    //     this.props.navigation.navigate('Login')
    // }
    static navigationOptions = {
        header: null,
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <View>
                    {/* <ImageHeader
                        icon={<Icon
                            name="menu"
                            size={45}
                            color="#10408a"
                            underlayColor=""
                        />}
                        title="Meny"
                    /> */}
                </View>

                <TouchableOpacity
                    onPress={() => navigate('SelectInterpreterScreen')} style={styles.section}>
                    {/* <Image
                        source={require('../assets/stopwatch.png')}
                    /> */}
                    <Text
                        style={styles.drawerItem}
                    >
                        TolkNu
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigate('ContactScreen')} style={styles.section}>
                    {/* <Image style={styles.icon}
                        source={require('../assets/call.png')}
                    /> */}
                    <Text
                        style={styles.drawerItem}
                    >
                        Kontakt
                        </Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.logoutSection}>
                    {/* <Image
                        style={styles.icon}
                        source={require('../assets/logouticon.png')}
                    /> */}
                    <Text
                        style={styles.drawerItem}
                    >
                        Logga ut
                        </Text>
                </TouchableOpacity>

                <View style={styles.versionContainer}>
                    <Text style={styles.version}>
                        Build: 
                    </Text>
                    <Text style={styles.version}>
                        Version: 
                    </Text>
                </View>
            </View >
        )
    }
}
/**
 *  creating style
 */
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#10408a',
    },
    drawerItem: {
        fontSize: 18,
        color: 'white',
        padding: 10,
        alignItems: 'center'
    },
    section: {
        display: 'flex',
        flexDirection: 'row',
        paddingVertical: 20,
        borderBottomColor: '#a9b1bc',
        borderBottomWidth: 0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoutSection: {
        display: 'flex',
        flexDirection: 'row',
        paddingVertical: 10,
        marginVertical: 10,
        justifyContent: 'center'
    },
    icon: {
        width: 50,
        height: 50,
    },
    versionContainer: {
        //padding: 20,
        position: 'absolute',
        top: Dimensions.get('window').height - 100,
        paddingHorizontal: 20,
        marginTop: 50,
        height: 50,
        //backgroundColor: 'red',
    },
    version: {
        color: 'white'
    }
})


export default DrawerContainer
