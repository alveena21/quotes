
import {
    createAppContainer,
    createStackNavigator,
    createDrawerNavigator
} from 'react-navigation';

import { LandingPage } from '../container';
import DrawerContent from './drawer'
const AppStack = createStackNavigator({
    LandingPage: LandingPage
},
    {
        initialRouteName: 'LandingPage'
    }
)


const DrawerStack = createDrawerNavigator(
    {
        Main: AppStack
    },
    {
        contentComponent: DrawerContent,
        // initialRouteName: 'SelectInterpreterScreen'
    }
);
export default createAppContainer(DrawerStack)
